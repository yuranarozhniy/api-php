<?php


class ModelAddress extends PDO{

    const USER = 'root';
    const PASS = 'root';

    protected $rules = [
        'label'       => 'string|max:100',
        'street'      => 'string|max:100',
        'housenumber' => 'string|max:10',
        'postalcode'  => 'string|max:6',
        'city'        => 'alpha|max:100',
        'country'     => 'alpha|max:100',

    ];

    protected $validateErrors = [];

    public function __construct()
    {
        $dsn = "mysql:host=localhost;dbname=rest";
        $opt = array(
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );
        parent::__construct($dsn, static::USER, static::PASS, $opt);
    }

    public function getAll()
    {
        return $this->query('SELECT * FROM address')->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getOne($id)
    {
        return $this->query('SELECT * FROM address WHERE addressId = ' . $id)->fetch(PDO::FETCH_ASSOC);
    }

    public function save($array)
    {
        $array = $this->checkFields($array);
        if (!$this->validate($array) || count($array) < 1) {
            return $this->validateErrors;
        }
        $fields = implode(',', array_keys($array));
        $this->prepare("INSERT INTO address ($fields) VALUES ( :".implode(", :",array_keys($array)).")")->execute($array);
        $id = $this->lastInsertId();

        return $this->getOne($id);
    }

    public function update($id, $array)
    {
        $array = $this->checkFields($array);
        if (!$this->validate($array) || count($array) < 1) {
            return $this->validateErrors;
        }

        $query = 'UPDATE address SET ';
        $values = array();
        foreach ($array as $name => $value) {
            $query .= ' '.$name.' = :'.$name.',';
            $values[':'.$name] = $value;
        }
        $query = substr($query, 0, -1);
        $query .= " WHERE addressId = " . $id;
        $this->prepare($query)->execute($values);

        return $this->getOne($id);
    }

    protected function validate($data)
    {
        $rules = [];
        foreach ($data as $key=>$value) {
            $rules[$key] = (is_string($this->rules[$key])) ? explode('|', $this->rules[$key]) : $this->rules[$key];
        }

        foreach ($rules as $key=>$value) {
            foreach ($value as $item) {
                switch ($item) {
                    case 'string':
                        $this->validateString($data[$key], $key);
                        break;
                    case 'alpha':
                        $this->validateAlpha($data[$key], $key);
                        break;
                    case stristr($item, 'max'):
                        $this->validateLength($data[$key], $key, $item);
                        break;
                }
            }
        }
        if (!empty($this->validateErrors)) {
            $this->validateErrors['error'] = true;
            return false;
        }

        return true;
    }

    protected function validateString($str, $field)
    {
        if (!is_string($str))
            $this->validateErrors = ["$field is not string please check field type"];

    }

    protected function validateAlpha($str, $field)
    {
        if (!ctype_alpha($str))
            $this->validateErrors = ["$field have symbols different from letters"];
    }

    protected function validateLength($str, $field, $rule)
    {
        $length = preg_replace("/[^0-9]/", '', $rule);
        if (strlen($str) > $length)
            $this->validateErrors = ["$field have length bigger that $length"];
    }

    protected function checkFields($array)
    {
        foreach ($array as $key=>$value) {
            if (!array_key_exists($key,$this->rules)) {
                unset($array[$key]);
            }
        }

        return $array;
    }
}