<?php
include "ModelAddress.php";

class RestAPI
{
    protected $method = '';

    protected $arguments = [];

    protected $connection;

    protected $response;


    public function __construct($request) {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");

        $this->arguments = explode('/', rtrim($request, '/'));
        if (array_key_exists(0, $this->arguments) && !is_numeric($this->arguments[0])) {
            $this->param = array_shift($this->arguments);
        }

        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
        $this->connection = new ModelAddress();

        switch($this->method) {
            case 'POST':
                if (count($this->arguments) > 1) {
                    $this->_response('Invalid Method', 405);
                    break;
                }
                $this->response = $this->_saveData($_POST);
                break;
            case 'GET':
                $this->response = $this->_getData();
                break;
            case 'PATCH':
            case 'PUT':
                if (count($this->arguments) < 2) {
                    $this->_response('Invalid Method', 405);
                    break;
                }
                $put = array();
                parse_str(file_get_contents('php://input'), $put);
                $this->response = $this->_updateData($put);
                break;
            default:
                $this->_response('Invalid Method', 405);
                break;
        }
    }

    public function start() {
        if (!empty($this->response) && array_key_exists('error', $this->response)) {
            return $this->_response($this->response, 400);
        } elseif (!empty($this->response)) {
            return $this->_response($this->response);
        }
        return $this->_response("No Found", 404);
    }

    private function _response($data, $status = 200) {
        header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
        return json_encode($data);
    }

    private function _getData()
    {
        if (!array_key_exists(1, $this->arguments)) {
            return $this->response = $this->connection->getAll();
        } else {
            return  $this->connection->getOne($this->arguments[1]);
        }
    }

    private function _saveData($params)
    {
        return $this->connection->save($params);
    }

    private function _updateData($params)
    {
        return $this->connection->update($this->arguments[1], $params);
    }


    private function _requestStatus($code) {
        $status = array(
            200 => 'OK',
            201 => 'Created',
            400 => 'Bad Request',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code])?$status[$code]:$status[500];
    }

}