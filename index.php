<?php
include 'RestAPI.php';

try {
    $api = new RestAPI($_SERVER['PATH_INFO']);
    echo $api->start();
} catch (Exception $e) {
    echo json_encode(['error' => $e->getMessage()]);
}
?>